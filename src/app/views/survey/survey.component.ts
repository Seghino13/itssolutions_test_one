import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {
  formData: FormGroup;
  submitted = false;
  success = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loadForm();
  }
  loadForm() {
    this.formData = this.formBuilder.group({
      employee: [ '' ],
      email: [ '', [ Validators.email] ],
      id: [ '', [Validators.required] ],
      city: [ '', [Validators.required] ],
      locality: [ '', [Validators.required] ],
      neighborhood: [ '', [Validators.required] ],
      address: [ '', [Validators.required] ],
      question: [ '', [Validators.required] ]
    });
  }
  get f() { return this.formData.controls; }
  validate() {
    this.submitted = true;
    if (this.formData.valid) {
      this.success = true;
      this.formData.reset();
    }
  }

}
